FROM python:3.7-alpine
LABEL Author="Igor Schouten"

ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip && pip install pipenv

COPY ./Pipfile.lock /Pipfile.lock
COPY ./Pipfile /Pipfile

RUN pipenv install --deploy --system --ignore-pipfile

RUN mkdir /app
WORKDIR /app
COPY ./app /app

RUN adduser -D user
USER user
